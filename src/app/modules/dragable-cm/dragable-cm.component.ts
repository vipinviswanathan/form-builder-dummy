import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dragable-cm',
  templateUrl: './dragable-cm.component.html',
  styleUrls: ['./dragable-cm.component.scss']
})
export class DragableCmComponent implements OnInit {
  dragl = [
    {
      name: 'short text',
      config: {
        type: 'short_text'
      }
    },
    {
      name: 'dropdown',
      config: {
        type: 'dropdown'
      }
    },
    {
      name: 'link',
      config: {
        type: 'link'
      }
    },
    {
      name: 'email',
      config: {
        type: 'email'
      }
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
