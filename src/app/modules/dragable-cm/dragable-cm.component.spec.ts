import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragableCmComponent } from './dragable-cm.component';

describe('DragableCmComponent', () => {
  let component: DragableCmComponent;
  let fixture: ComponentFixture<DragableCmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragableCmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragableCmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
