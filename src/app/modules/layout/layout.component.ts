import { Component, OnInit } from '@angular/core';
import { moveItemInArray, CdkDragDrop, copyArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  ngOnInit(): void {

  }
  title = 'form-builder';

  // dragl = [
  //   'text',
  //   'dropdown',
  //   'link',
  //   'email'
  // ];

  settings:string = null;

  dragl = [
    {
      name: 'short text',
      config: {
        type: 'short_text'
      }
    },
    {
      name: 'dropdown',
      config: {
        type: 'dropdown'
      }
    },
    {
      name: 'link',
      config: {
        type: 'link'
      }
    },
    {
      name: 'email',
      config: {
        type: 'email'
      }
    }
  ];

  dropl = [
    {
      sectionId: "section"+1,
      sectionName: 'basicdetails',
      controls: [

      ]
    },
    {
      sectionId: "section"+2,
      sectionName: 'basicdetails',
      controls: [
        {
          name: 'email',
          config: {
            type: 'email'
          }
        }
      ]
    }

  ];

  section=this.dropl[0];


  drop(event: CdkDragDrop<any[]>) {
    console.log(event);
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
        console.log(event.container.data[event.currentIndex].config.type);
        this.settings=event.container.data[event.currentIndex].config.type;
      // transferArrayItem(event.previousContainer.data,
      //   event.container.data,
      //   event.previousIndex,
      //   event.currentIndex);
    }
  }

  subformmit(event) {
    console.log(event);
    console.log(this.dropl);
  }

  edit(control) {
    this.settings = control.config.type;
  }

  addSection() {
    this.dropl.push({sectionId: "section"+(this.dropl.length+1), sectionName: "new", controls: []});
    console.log(this.dropl)
  }

  removeSection(event, sectionId) {
    event.stopPropagation();
    const index = this.dropl.findIndex(section => section.sectionId == sectionId);
    if (index > -1) {
      this.dropl.splice(index, 1);
    }
  }

  test(event) {
    console.log(event);
  }

  dropGroup(event) {
    console.log('dropgroup', event);
  }

  getConnectedList(): any[] {
    return this.dropl.map(x => `${x.sectionId}`);
  }

}
