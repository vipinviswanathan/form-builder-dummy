import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropableCmComponent } from './dropable-cm.component';

describe('DropableCmComponent', () => {
  let component: DropableCmComponent;
  let fixture: ComponentFixture<DropableCmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropableCmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropableCmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
