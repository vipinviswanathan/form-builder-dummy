import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, copyArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-dropable-cm',
  templateUrl: './dropable-cm.component.html',
  styleUrls: ['./dropable-cm.component.scss']
})
export class DropableCmComponent implements OnInit {
  dropl = [
    {
      sectionId: "section"+1,
      sectionName: 'basicdetails',
      controls: [

      ]
    },
    {
      sectionId: "section"+2,
      sectionName: 'basicdetails',
      controls: [
        {
          name: 'email',
          config: {
            type: 'email'
          }
        }
      ]
    }

  ];
  settings:string = null;
  constructor() { }

  ngOnInit() {
  }

  drop(event: CdkDragDrop<any[]>) {
    console.log(event);
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      copyArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
        console.log(event.container.data[event.currentIndex].config.type);
        this.settings=event.container.data[event.currentIndex].config.type;
      // transferArrayItem(event.previousContainer.data,
      //   event.container.data,
      //   event.previousIndex,
      //   event.currentIndex);
    }
  }


  subformmit(event) {
    console.log(event);
    console.log(this.dropl);
  }

  edit(control) {
    this.settings = control.config.type;
  }

  addSection() {
    this.dropl.push({sectionId: "section"+(this.dropl.length+1), sectionName: "new", controls: []});
    console.log(this.dropl)
  }

  removeSection(event, sectionId) {
    event.stopPropagation();
    const index = this.dropl.findIndex(section => section.sectionId == sectionId);
    if (index > -1) {
      this.dropl.splice(index, 1);
    }
  }

  test(event) {
    console.log(event);
  }

  dropGroup(event) {
    console.log('dropgroup', event);
  }

  getConnectedList(): any[] {
    return this.dropl.map(x => `${x.sectionId}`);
  }

}
