import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortTextSettingsComponent } from './short-text-settings.component';

describe('ShortTextSettingsComponent', () => {
  let component: ShortTextSettingsComponent;
  let fixture: ComponentFixture<ShortTextSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortTextSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortTextSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
