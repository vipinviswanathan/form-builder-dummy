import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BrowserModule } from '@angular/platform-browser';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button'

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ShortTextSettingsComponent } from './short-text-settings/short-text-settings.component';
import { DragableCmComponent } from './modules/dragable-cm/dragable-cm.component';
import { DropableCmComponent } from './modules/dropable-cm/dropable-cm.component';
import { LayoutComponent } from './modules/layout/layout.component';

@NgModule({
  declarations: [
    AppComponent,
    ShortTextSettingsComponent,
    DragableCmComponent,
    DropableCmComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatExpansionModule,
    FormsModule,
    MatButtonModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
